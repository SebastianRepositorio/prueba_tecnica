import { createAll, cleanConsole } from './data';
const companies = createAll();

cleanConsole(1, companies);
console.log('---- EXAMPLE 1 --- ', 'Put here your function');
//deleteUndefinedValue(companies);
export function deleteUndefinedValue(companies) {
    for (let index = 0; index < companies.length; index++) {
        const company = companies[index];
        company.name = company.name.charAt(0).toUpperCase() + company.name.slice(1);
    };
    for (let index = 0; index < companies.length; index++) {
        const company = companies[index];
        for (let index = 0; index < company.users.length; index++) {
            const user = company.users[index];
            if (!user.firstName) {
                user.firstName = '';
            } else {
                user.firstName = user.firstName.charAt(0).toUpperCase() + user.firstName.slice(1);
            };
            if (!user.lastName) {
                user.lastName = '';
            } else {
                user.lastName = user.lastName.charAt(0).toUpperCase() + user.lastName.slice(1);
            };
        };
        company.users.sort(function (a, b) {
            return a.firstName.localeCompare(b.firstName);
        });
    };
    var array = companies.sort((a, b) => {
        if (a.users.length > b.users.length) {
            return 1;
        };
        if (a.users.length < b.users.length) {
            return -1;
        };
        return 0;
    });
    return array;
};

// -----------------------------------------------------------------------------
// INSTRUCCIONES EN ESPAÑOL

// Crear una función tomando la variable "companies" como parámetro y reemplazando
// todos los valores "undefined" en "usuarios" por un string vacío.
// El nombre de cada "company" debe tener una letra mayúscula al principio, así como
// el apellido y el nombre de cada "user".
// Las "companies" deben ordenarse por su total de "user" (orden decreciente)
// y los "users" de cada "company" deben aparecer en orden alfabético.

// -----------------------------------------------------------------------------
// INSTRUCTIONS IN ENGLISH

// Create a function taking the variable "companies" as a parameter and replacing
// all values "undefined" in "users" by an empty string.
// The name of each "company" must have a capital letter at the beginning as well as
// the last name and first name of each "user".
// The "companies" must be sorted by their number of "user" (decreasing order)
// and the "users" of each "company" must be listed in alphabetical order.

// -----------------------------------------------------------------------------
// INSTRUCTIONS EN FRANÇAIS

// Créer une fonction prenant en paramètre la variable "companies" et remplaçant
// toutes les valeurs "undefined" dans les "users" par un string vide.
// Le nom de chaque "company" doit avoir une majuscule au début ainsi que
// le nom et le prénom de chaque "user".
// Les "companies" doivent être triées par leur nombre de "user" (ordre décroissant)
// et les "users" de chaque "company" doivent être classés par ordre alphabétique.
