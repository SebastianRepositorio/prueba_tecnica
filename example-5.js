import {cleanConsole, createAll} from './data';
import {newTableUser} from './example-4';
const companies = createAll();

cleanConsole(5, companies);
console.log('---- EXAMPLE 5 --- ', 'Put here your function');
const users = newTableUser(companies);
calculateAverages(users);
export function calculateAverages(users) {
    let totalUsers = 0;
    let ageUsers = 0;
    let averageAgeUsers = 0;
    let hasCar = 0;
    let ageWithCar = 0;
    let averageWithCar = 0;
    for (let i = 0; i < users.length; i++) {
        const user = users[i];
        totalUsers++;
        ageUsers = ageUsers + user.age;
        if (user.car) {
            hasCar++;
            ageWithCar = ageWithCar + user.age;
        };
    };
    averageAgeUsers = ageUsers/totalUsers;
    averageWithCar = ageWithCar/hasCar;
    let obj = {
        size: totalUsers,
        average: averageAgeUsers,
        hasCar: hasCar,
        averageWithCar: averageWithCar
    };
    return obj;

};

// -----------------------------------------------------------------------------
// INSTRUCCIONES EN ESPAÑOL

// Use la función creada en el ejemplo 4 para crear una nueva función tomando
// como parámetro la variable "companies" y devuelve un nuevo objeto con los
// siguientes atributos:
//     'size' => total de "users"
//     'average' => edad promedio de "users"
//     'hasCar' => total de "users" propietarios de un carro
//     'averageWithCar' => edad promedio de los "users" con un carro

// -----------------------------------------------------------------------------
// INSTRUCTIONS IN ENGLISH

// Use the function created in example 4 to create a
// new function taking as parameter the "companies" variable and returning
// a new object with the following attributes:
//     'size' => number of "users"
//     'average' => average age of "users"
//     'hasCar' => number of "users" owning a car
//     'averageWithCar' => average age of users with a car

// -----------------------------------------------------------------------------
// INSTRUCTIONS EN FRANÇAIS

// Utiliser la fonction créée dans l'exemple 4 pour créer une
// nouvelle fonction prenant en paramètre la variable "companies" et renvoyant
// un nouvel objet avec les attributs suivants :
//     'size' => nombre de "users"
//     'average' => moyenne d'âge des "users"
//     'hasCar' => nombre de "users" possédant une voiture
//     'averageWithCar' => moyenne d'âge des "users" possédant une voiture
